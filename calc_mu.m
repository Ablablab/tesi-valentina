function mu = calc_mu(i, lambda, T, r)
    moltiplicatore = (1 - lambda) / (1 - lambda^T);
    somma = 0.0;
    
    for t = 1:T
        somma = somma + (lambda ^ (T - t)) * r(i, t);
    end
    mu = somma * moltiplicatore;
end