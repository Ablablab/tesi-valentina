function sigma_calcolata = calc_sigma(i,j, lambda, T, r)
    moltiplicatore = (1 - lambda) / (1 - lambda^T);
    somma = 0.0;
    
    for t = 1:T
        somma = somma + (lambda ^ (T - t)) * (r(i,t) - calc_mu(i, lambda, T, r)) * (r(j,t) - calc_mu(j, lambda, T, r));
    end
    sigma_calcolata = somma * moltiplicatore;
end