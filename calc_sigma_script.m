[T , n_asset] = size(data);
sigma_data = zeros([n_asset , n_asset]);

% T numero di tempi 61
% n_asset 4

r = data'; % data (61 x 4) , r ( 4 x 61 )

for i = 1:n_asset % i \in 1,4
    for j = 1:n_asset % j \in 1,61
        sigma_data(i , j)    = calc_sigma(i,j, lambda, T, r);
    end
end